package it.unibo.oop.lab.enum1;

import static org.junit.Assert.*;

import org.junit.Test;

import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
    @Test
    public static void main(final String... args) {
        /*
         * [TEST DEFINITION]
         * 
         * By now, you know how to do it
         */
        // TODO
    	final SportSocialNetworkUserImpl<User> u1 = new SportSocialNetworkUserImpl<>("Matteo", "Scucchia", "mat1", 19);
    	final SportSocialNetworkUserImpl<User> u2 = new SportSocialNetworkUserImpl<>("Mariano", "Cladara", "merio", 20);
    	
    	u1.addSport(Sport.BASKET);
    	u2.addSport(Sport.SOCCER);
    	u1.addSport(Sport.BASKET);
    	System.out.println(u1);
    	System.out.println(u2);
    	System.out.println(u1.hasSport(Sport.SOCCER));
    	assertFalse(u1.hasSport(Sport.SOCCER));
    	assertTrue(u2.hasSport(Sport.SOCCER));
    	
    }
}
